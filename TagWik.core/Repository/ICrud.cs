﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace TagWik.Core.Repository
{
    public interface ICrud<T>
    {
        T Get(string id);
        IEnumerable<T> GetAll();
        IEnumerable<T> Where(Expression<Func<T, bool>> predicate, bool showDeleted = false);
        T Insert(T o);
        void Save();
        void Delete(T o);
    }
}
