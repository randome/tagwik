﻿using TagWik.Core.Model;

namespace TagWik.Core.Repository
{
    public interface IContentRepository : ICrud<ContentBase>
    {
    }
}
