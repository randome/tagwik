using System;
using System.Collections.Generic;
using DreamSongs.MongoRepository;

namespace TagWik.Core.Model
{
    public class ContentBase : Entity
    {
        /// <summary>
        /// Id of the object, will be used as unique url
        /// </summary>
        //public string Id { get; set; }
        /// <summary>
        /// List of tags describing the content
        /// </summary>
        public IList<string> Tags { get; set; }
        /// <summary>
        /// Time stamp when the content was created
        /// </summary>
        public DateTime CreatedDate{ get; set; }
        /// <summary>
        /// Creator of content
        /// </summary>
        public string Author { get; set; }
        /// <summary>
        /// Version of the current record
        /// </summary>
        public float Version { get; set; }
   
        public ContentBase()
        {
            CreatedDate = DateTime.Now;
            Version = 0f;
            //Tags = new string[]{};
        }
    }
}