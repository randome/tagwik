﻿using System.Collections.Generic;
using TagWik.Core.Model;

namespace TagWik.Core.Model
{ 
    public class Document : ContentBase
    {
        /// <summary>
        /// Id of a file saved in the database
        /// </summary>
        public IList<string> FileIds { get; set; }
        /// <summary>
        /// Type of the document
        /// </summary>
        public DocumentType Type { get; set; }
        /// <summary>
        /// Title of the document
        /// </summary>
        public string Title { get; set; }

        public Document()
        {
            FileIds = new List<string>();
        }

    }

    public enum DocumentType
    {
        Pdf,
        Word
    }
}
