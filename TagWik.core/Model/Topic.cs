﻿using TagWik.Core.Model;

namespace TagWik.Core.Model
{
    public class Topic : ContentBase
    {
        /// <summary>
        /// Title of the topic
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// Body of the content
        /// </summary>
        public string Body { get; set; }
    }
}