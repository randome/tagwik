﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using TagWik.Domain.Model;
using TagWik.Domain.Storage;
using TagWik.Models;

namespace TagWik.Controllers
{
    public class ContentController : Controller
    {
        private readonly IContentRepository _repository;

        public ContentController(IContentRepository repository)
        {
            _repository = repository;
        }

        [HttpPost]
        public ActionResult AddTopic(Topic topic)
        {
            if (!ModelState.IsValid)
                return RedirectToAction("Index", "Home");

            // TODO : Remove this when using ViewModel
            topic.Tags = topic.Tags[0].Split(' ').ToList();

            topic.Author = (string) Session["User"];
            _repository.SaveTopic(topic);
            
            return RedirectToAction("Details", new {id = topic.Id});
        }

        [HttpPost]
        public ActionResult AddDocument(DocumentView document, string id)
        {
          //  if (!ModelState.IsValid)
          //      return RedirectToAction("Index", "Home");
            if (String.IsNullOrEmpty(id))
                return new EmptyResult();

            // TODO : improve support for multiple files

            // Update the document that already contains doc id's
            _repository.SaveDocument(new Document()
                                         {
                                             Id = id,
                                             Author = (string) Session["User"],
                                             Title =  document.Title,
                                             Tags = document.Tags
                                         });

           return Content(id, "text/plain");//RedirectToAction("Details", new {id});
        }

        [HttpPost]
        public ActionResult UploadDocument(int? chunk, string name, string id)
        {
            var fileUpload = Request.Files[0];

            Document doc = null;
            if(!String.IsNullOrEmpty(id))
                doc = _repository.GetDocument(id);

            if(doc == null)
                doc = new Document();

            if (fileUpload != null)
            {
                var fileId = _repository.SaveFile(fileUpload.InputStream, fileUpload.FileName);
                doc.FileIds.Add(fileId);
            }
            else
                return Content("Failed", "text/plain");

            var docID = _repository.SaveDocument(doc);
            return Content(docID, "text/plain");
        }

        public ActionResult Details(string id)
        {
            var topic = _repository.GetTopic(id);

            //todo : fix this check
            if (topic == null)
            {
                var doc = _repository.GetDocument(id);
                ViewBag.Title = doc.Title;
                return View("Details/Document", doc);
            }

            ViewBag.Title = topic.Title;
            return View(topic);
        }
    }
}