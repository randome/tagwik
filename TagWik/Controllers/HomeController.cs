﻿using System.Web.Mvc;
using TagWik.Domain.Storage;

namespace TagWik.Controllers
{
    public class HomeController : Controller
    {
        private readonly IContentRepository _repository;
        //
        // GET: /Home/
        public HomeController(IContentRepository repository)
        {
            _repository = repository;
        }

        public ActionResult Index()
        {
            Session["User"] = "Dejan Stokanic";
            return View(_repository.GetTopics());
        }

        public ActionResult Favorites()
        {
            return View();
        }

        public ActionResult Topics()
        {
            return View();
        }
    }
}
