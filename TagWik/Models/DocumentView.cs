﻿using System.Collections.Generic;

namespace TagWik.Models
{
    public class DocumentView
    {
        public string Title { get; set; }
        public IList<string> Tags { get; set; }
    }
}