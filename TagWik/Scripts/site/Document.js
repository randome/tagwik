﻿$(function () {
    var documentId;
    var uploader = new plupload.Uploader({
        runtimes: 'html5,flash',
        browse_button: 'pickfiles',
        container: 'filecontainer',
        max_file_size: '100mb',
        drop_element: 'filedrop',
        filters: [
            { title: "Images", extensions: "jpg,png" },
            { title: "Documents", extensions: "txt,doc,docx,xsl,csv" }
        ],
        url: 'Content/UploadDocument'
    });

    uploader.bind('Init', function(up, params) {
        console.log("Pl Upload using :" + params.runtime);
    });

    $("#uploadfiles").click(function(e) {
        uploader.start();
        e.preventDefault();
    });

    uploader.init();

    uploader.bind('FilesAdded', function(up, files) {
        $.each(files, function(i, file) {
            $("#filelist").append(
                '<div id="' + file.id + '">' + file.name + ' <b class="pull-right">' + plupload.formatSize(file.size) + '</b>' +
                    '<div class="progress" style="height:5px;margin-bottom:0px;"><div class="bar" style="height:3px;"></div></div>' +
                    '<div class="error-upload" style="margin-bottom:5px;"></div>' +
                '</div>'
            );
        });
        up.refresh();
    });

    uploader.bind('UploadProgress', function(up, file) {
        // $('#' + file.id + " b").html(plupload.formatSize(file.loaded) + " / " + plupload.formatSize(file.size));
        $('#' + file.id + " .bar").css("width", file.percent + "%");
    });

    uploader.bind('UploadComplete', function (up, files) {
        var form = $('#addDocument form');
        console.log(form.serialize());
        $.ajax({
            type: "POST",
            url: form.attr("action"),
            data: form.serialize() + "&id=" + documentId,
            success: function (result) {
                location.href = "/" + result;
            }
        });
    });

    uploader.bind('Error', function(up, err) {
        $('#' + err.file.id + " .error-upload").text(err.code + " : " + err.message);
        $('#' + err.file.id + " .progress").addClass("progress-danger");
        uploader.stop();
        up.refresh(); // Reposition Flash/Silverlight
    });

    uploader.bind('FileUploaded', function(up, file, response) {
        //$('#' + file.id + " b").html("Done");
        if (documentId != null && documentId != response)
            console.log("Wrong DOC IDs! current:'" + response + "' previous:'" + documentId + "'");
        documentId = response.response;

        console.log(file.name + " uploaded. Doc id : " + documentId);
    });

    var title = $('#addDocument form #title');
    $(title).keyup(function() {
        if (title.val() == "")
            $('#addDocument h3').html('New Document');
        else
            $('#addDocument h3').html(title.val());
    });

    $("#addDocument input[type=button]").click(function(e) {
        uploader.start();        
        e.preventDefault();
    });
});