﻿$(function () {
    var title = $('#addTopic form #Title');

    $('#Body').wysihtml5();
    
    $('#addTopic input[type=button]').click(function () {
        $('#addTopic form').submit();
    });

    $(title).keyup(function () {
        if (title.val() == "")
            $('#addTopic h3').html('New Topic');
        else
            $('#addTopic h3').html(title.val());
    });

    $('a[data-dismiss]=modal').click(function () {
        // todo : clear form
    });
});