﻿using System.Configuration;
using MongoDB.Driver;
using MongoDB.Driver.GridFS;

namespace TagWik.Domain
{
    public class Mongo<T> where T : class
    {
        public Mongo()
        {
            var connection =
                new MongoConnectionStringBuilder(ConfigurationManager.ConnectionStrings["MongoDB"].ConnectionString);

            MongoServer server = MongoServer.Create(connection);
            MongoDatabase database = server.GetDatabase(connection.DatabaseName);
            
            Collection = database.GetCollection<T>(typeof(T).Name.ToLower());
            GridFs = database.GridFS;
        }

        public MongoCollection<T> Collection { get; private set; }
        public MongoGridFS GridFs { get; private set; }
    }
}
