﻿using System.Collections.Generic;

namespace TagWik.Domain.Model
{ 
    public class Document : Entity
    {
        public IList<string> FileIds { get; set; }
        public DocType Type { get; set; }
        public string Title { get; set; }

        public Document()
        {
            FileIds = new List<string>();
        }

    }

    public enum DocType
    {
        Pdf,
        Word
    }
}
