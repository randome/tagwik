using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace TagWik.Domain.Model
{
    public class Entity
    {
        /// <summary>
        /// Id of the object, will be used as unique url
        /// </summary>
        public string Id { get; set; }
        /// <summary>
        /// List of tags describing the content
        /// </summary>
        [Required]
        public IList<string> Tags { get; set; }
        public DateTime UploadTime { get; set; }
        public string Author { get; set; }
        /// <summary>
        /// Version of the current record
        /// </summary>
        public int Version { get; set; }
   
        public Entity()
        {
            UploadTime = DateTime.Now;
            Version = 0;
            //Tags = new string[]{};
        }
    }
}