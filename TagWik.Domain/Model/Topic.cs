﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace TagWik.Domain.Model
{
    public class Topic : Entity
    {
        [Required]
        public string Title { get; set; }
        [AllowHtml]
        public string Body { get; set; }
    }
}