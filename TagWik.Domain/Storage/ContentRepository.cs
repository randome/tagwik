using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using MongoDB.Driver.GridFS;
using TagWik.Domain.Model;

namespace TagWik.Domain.Storage
{
    public class ContentRepository : IContentRepository
    {
        private readonly MongoCollection<Topic> _topics;
        private readonly MongoCollection<Document> _documents;
        private readonly MongoGridFS _gridFs;

        public ContentRepository()
        {
            _topics = new Mongo<Topic>().Collection;
            _documents = new Mongo<Document>().Collection;
            _gridFs = new Mongo<Document>().GridFs; // Generic is unecessary
        }

        public string SaveTopic(Topic topic)
        {
            topic.Id = GenerateId();
            _topics.Save(topic);
            return topic.Id;
        }

        public IEnumerable<Topic> GetTopics()
        {
            return _topics.FindAll()
                .SetLimit(50).SetSortOrder(SortBy.Descending("UploadTime"));
        }

        public Topic GetTopic(string id)
        {
            return
                _topics.FindOneById(id);
        }

        public string SaveDocument(Document document)
        {
            if (String.IsNullOrEmpty(document.Id))
                document.Id = GenerateId();
            // TODO : check that the docID alrady exists
            _documents.Save(document);
            return document.Id;
        }

        public string SaveFile(Stream stream, string fileName)
        {
            return _gridFs.Upload(stream, fileName).Id.ToString();
        }

        public Document GetDocument(string id)
        {
            var doc = _documents.FindOneById(id);

            foreach (var fileId in doc.FileIds)
            {
                _gridFs.FindOneById(fileId);
            }

            return doc;
            //TODO: Find all the relates file ids
        }

        protected string GenerateId()
        {
            // TODO: Check if ID already exists
            const string chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
            var random = new Random();
            var result = new string(
                Enumerable.Repeat(chars, 4)
                    .Select(s => s[random.Next(s.Length)])
                    .ToArray());

            return result;
        }
    }
}