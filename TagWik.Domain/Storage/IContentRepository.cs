using System.Collections.Generic;
using System.IO;
using TagWik.Domain.Model;

namespace TagWik.Domain.Storage
{
    public interface IContentRepository
    {
        string SaveTopic(Topic topic);
        IEnumerable<Topic> GetTopics();
        Topic GetTopic(string id);
        string SaveDocument(Document document);
        string SaveFile(Stream stream, string fileName);
        Document GetDocument(string id);
    }
}