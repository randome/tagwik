﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using DreamSongs.MongoRepository;
using TagWik.Core.Model;
using TagWik.Core.Repository;

namespace TagWik.Data.MongoDB
{
    public class ContentRepository : MongoRepository<ContentBase>, IContentRepository
    {     
        public ContentBase Get(string id)
        {
            return GetById(id);
        }
        
        public IEnumerable<ContentBase> GetAll()
        {
            return All();
        }
        
        public IEnumerable<ContentBase> Where(Expression<Func<ContentBase, bool>> predicate, bool showDeleted = false)
        {
            //ToDo : implement showDeleted
            return All(predicate);
        }

        public ContentBase Insert(ContentBase o)
        {
            return Add(o);
        }

        public void Save()
        {
            throw new NotSupportedException();
        }
        
        /*
        public void Delete(ContentBase o)
        {
            throw new NotImplementedException();
        }*/
         
    }
}
